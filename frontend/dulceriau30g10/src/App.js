import React from 'react';
import './App.css';
import {BrowserRouter as Router, Routes, Route} from 'react-router-dom'


import Background from './components/Background'
import Home from './components/Home'
//import CompProductosListar from './components/CompProductosListar'
//import Sidebar from './components/sidebar';



function App() {
  return (
    <div>
      <Background/>
      <Router>
      <Routes>
        <Route path='/' element={<Home/>}/>
        </Routes>
    </Router>
    </div>    
    
  );
}

export default App;

