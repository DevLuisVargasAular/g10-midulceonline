import React from 'react';

function Background() {
    return (
        <div>
            <div style={{
                backgroundImage: `url("https://img2.rtve.es/i/?w=1600&i=1628606892247.jpg")`,
                height: `100%`,
                width: `100%`,
                position: `absolute`,

            }}>
                <div className='mask' style={{ backgroundColor: 'rgba(0, 0, 0, 0.3)', 
                height: `100%`,
                width: `100%`,
                position: `absolute`
             }}>
                    <div className='d-flex justify-content-center align-items-center h-100'>
                        <p className='text-white mb-0'>MY CANDY <br/> online</p>
                        
                    </div>    
                </div>

            </div>

        </div>
    );
}
export default Background;