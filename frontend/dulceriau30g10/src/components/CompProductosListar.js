//import React from 'react'
import axios from 'axios'

//HOOKS --> GANCHOS --> Gestionar los estados del componente
import React, { useState, useEffect } from 'react'

import { Link } from 'react-router-dom'

const URI = 'http://localhost:4000/'

const [rutas, setRuta] = useState([])
    useEffect(() => {
        //getRutasvuelos()
    }, [])


const CompRutasvuelosListar = () => {    
//METODOS DEL COMPONENTE
    //METODO PARA LISTAR LAS RUTAS DE VUELO
    const getRutasvuelos = async () => {
        const res = await axios.get(`${URI}listarAll/`)
        const dataConvert = []
        for (var i in res.data) {
            dataConvert.push(i, res.data[i]);
        }
        //console.log(res.data)
        setRuta(dataConvert[1]);
        //setRuta(res.data.message)
        //setRuta(res.data.result)
    }
    
    //METODO PARA ELIMINAR LAS RUTAS DE VUELO
    const deleteRutasvuelos = async (id) => {
        axios.delete(`${URI}delete/${id}`)
        getRutasvuelos()
    }

    return (
        <div>
            <div>
                <div>
                    <Link to={'/crear'}>
                        <span icon="fa-solid fa-plus"></span>
                    </Link>
                    <table>
                        <thead>
                            <tr>
                                <th>CODPRODUCTO</th>
                                <th>NOMBRE</th>
                                <th>PRESENTACION</th>
                                <th>PRECIO</th>
                                <th>UNIDADES</th>
                                <th>ACCIONES</th>
                            </tr>
                        </thead>
                        <tbody>
                            {rutas.map((element) => {
                                return (
                                    <tr key={element._id}>
                                        <td>{element.codProducto}</td>
                                        <td>{element.nombre}</td>
                                        <td>{element.presentacion}</td>
                                        <td>{element.precio}</td>
                                        <td>{element.unidades}</td>
                                        <td>
                                            <Link to={`/editar/${element._id}`} className="fa-regular fa-pen-to-square"></Link>
                                            &nbsp;&nbsp;&nbsp;
                                            <Link onClick={() => deleteRutasvuelos(element._id)} className="fa-solid fa-eraser" />
                                        </td>
                                    </tr>
                                );
                            }
                            )}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    )
}

export default CompRutasvuelosListar