const {Router} = require('express');
const router = Router();

// Rutas para el carshop
var controllerCarShop = require('../src/controllers/controllerCarShop');

router.get("/list/products-cart", controllerCarShop.getProductosCar);
router.post("/create/products-cart", controllerCarShop.addProductoCar);

// Rutas para la entidad producto
var controllerProducto = require('../src/controllers/controllerProducto');

router.get('/prueba/producto',controllerProducto.prueba);
router.post('/crear/producto',controllerProducto.saveProducto);
router.get('/leer-producto/:id',controllerProducto.readProducto);
router.get('/listarall-producto/:id?',controllerProducto.listarProducto);
router.put('/update/producto/:id',controllerProducto.updateProducto);
router.delete('/delete/:id',controllerProducto.deleteProducto);
router.delete("/products-cart-delete/:productoId", controllerProducto.deleteProductoinCar);


//Rutas para la entidad cliente
var controllerCliente = require('../src/controllers/controllerCliente');

router.get('/prueba/cliente',controllerCliente.pruebaCliente);
router.post('/create/cliente',controllerCliente.saveCliente);
router.get('/listbyid/cliente/:id',controllerCliente.readCliente);
router.get('/listall/cliente/:id?',controllerCliente.listarCliente);
router.put('/update/cliente/:id',controllerCliente.updateCliente);
router.delete('/delete/cliente/:id',controllerCliente.deleteCliente);

//Rutas para la entidad Factura:

var controllerFactura = require('../src/controllers/controllerFactura');

router.get('/factura/prueba',controllerFactura.fprueba);
router.post('/create/factura',controllerFactura.saveFactura);
router.get('/listbyid/factura/:id',controllerFactura.readFactura);
router.get('/listall/factura/:id?',controllerFactura.listarFactura);
router.put('/update/factura/:id',controllerFactura.updateFactura);
router.delete('/delete/factura/:id',controllerFactura.deleteFactura);

//Rutas para la entidad Pedido:

var controllerPedido = require('../src/controllers/controllerPedido');

router.post('/create/pedido',controllerPedido.savePedido);
router.get('/listbyid/pedido/:id',controllerPedido.readPedido);
router.get('/listall/pedido/:id?',controllerPedido.listarPedido);
router.put('/update/pedido/:id',controllerPedido.updatePedido);
router.delete('/delete/pedido/:id',controllerPedido.deletePedido);

//Rutas para la entidad Usuario:

var controllerUsuario = require('../src/controllers/controllerUsuario');

router.post('/create/usuario',controllerUsuario.saveUsuario);
router.get('/listbyid/usuario/:id',controllerUsuario.readUsuario);
router.get('/listall/usuario/:id?',controllerUsuario.listarUsuario);
router.put('/update/usuario/:id',controllerUsuario.updateUsuario);
router.delete('/delete/usuario/:id',controllerUsuario.deleteUsusario);

//export
module.exports = router;
