const Usuario = require('../models/usuario');
//Metodos del CRUD

//Funcion crear:
function saveUsuario(req, res){
    var myUsuario = new Usuario(req.body);
    myUsuario.save((err, result)=>{
        res.status(200).send({message: result});
    });
}

//Funcion Leer Read:
function readUsuario(req,res){
    var idUsusario = req.params.id;
    Usuario.findById(idUsusario).exec((err, result)=>{
        if (err){
            res.status(500).send({message: 'Error al procesar la solicitud'});
        }else{
            if (!result){
                res.status(404).send({message: 'Valor incorrecto. No se pudo procesar'});
            } else {
                res.status(200).send({message: result});
            }
        }
    })

}

function listarUsuario(req,res){
    var idUsuario = req.params.id;
    if (!idUsuario){
        var result = Usuario.find({}).sort('nombreUser');
    }else{
        var result = Usuario.find({_id:idUsuario}).sort('nombreUser');
    }
    result.exec(function(err,result){
        if (err){
            res.status(500).send({message: 'Error al procesar la solicitud'});
        }else{
            if (!result){
                res.status(404).send({message: 'Valor incorrecto. No se pudo procesar'});
            } else {
                res.status(200).send({message: result});
            }
        }
    })
}
function updateUsuario(req,res){
    var idUsuario = req.params.id;
    Usuario.findOneAndUpdate({_id:idUsuario},req.body,{new:true},function(err,Usuario){
        if(err)
            res.send(err)
        res.json(Usuario)
    })
}
function deleteUsusario(req,res){
    var idUsuario = req.params.id;
    Usuario.findByIdAndDelete(idUsuario,function(err,Usuario){
        if(err){
            return res.json(500,{
                message: 'No se ha encontrado el Producto a eliminar'
            })
        }
        return res.json(Usuario);
    })
}

module.exports = {
    saveUsuario,
    readUsuario,
    listarUsuario,
    updateUsuario,
    deleteUsusario
}
