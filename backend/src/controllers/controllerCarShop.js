const carshop = require("../models/carshop");
const producto = require("../models/producto");

var addProductoCar = async (req, res) => {
  const { nombre, precio } = req.body;

  /* Nos fijamos si tenemos el producto */
  const estaEnProductos = await producto.findOne({ nombre });

  /* Nos fijamos si todos los campos vienen con info */
  const noEstaVacio = nombre !== "" && cantidad !== "" && precio !== "";

  /* Nos fijamos si el producto ya esta en el carrito */
  const estaEnElCarrito = await carshop.findOne({ nombre });

  /* Si no tenemos el producto */
  if (!estaEnProductos) {
    res.status(400).json({
      mensaje: "Este producto no se encuentra en nuestra base de datos",
    });

    /* Si nos envian algo y no esta en el carrito lo agregamos */
  } else if (noEstaVacio && !estaEnElCarrito) {
    const nuevoProductoEnCar = new Car({ nombre, precio, cantidad : 1 });

    /* Y actualizamos la prop isCar: true en nuestros productos */
    await producto.findByIdAndUpdate(
      estaEnProductos?._id,
      { isCarShop: true, nombre, precio },
      { new: true }
    )
      .then((producto) => {
        nuevoProductoEnCar.save();
        res.json({
          mensaje: `El producto fue agregado al carrito`,
          producto,
        });
      })
      .catch((error) => console.error(error));

    /* si esta en el carrito avisamos */
  } else if (estaEnElCarrito) {
    res.status(400).json({
      mensaje: "El producto ya esta en el carrito",
    });
  }
};

// controlador Obtenerproductos en el car
var getProductosCar = async (req, res) => {
    const productosCar = await carshop.find();
  
    if (productosCar) {
      res.json({ productosCar });
    } else {
      res.json({ mensaje: "No hay productos en el carrito" });
    }
  };
  


module.exports = {addProductoCar,getProductosCar};