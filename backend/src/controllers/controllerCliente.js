const { findById } = require('../models/cliente');
const Cliente = require('../models/cliente');

function pruebaCliente(req,res){
    res.status(200).send({
        message:'Probando la ejecucion del controlador Cliente'
    });
}
//CRUD=

//Create:
function saveCliente(req,res){
    var myCliente = new Cliente(req.body);
    myCliente.save((err, result)=>{
        res.status(200).send({message: result});
    });
}

//Read:
function readCliente(req,res){
    var idCliente = req.params.id;
    Cliente.findById(idCliente).exec((err, result)=>{
        if (err){
            res.status(500).send({message: 'Error al procesar la solicitud'});
        }else{
            if (!result){
                res.status(404).send({message: 'Valor incorrecto. No se pudo procesar'});
            } else {
                res.status(200).send({message: result});
            }
        }
    })
}

//Listar clientes:

function listarCliente(req,res){
    var idCliente = req.params.id;
    if (!idCliente){
        var result = Cliente.find({}).sort('nombre');
    }else{
        var result = Cliente.find({_id:idCliente}).sort('nombre');
    }
    result.exec(function(err,result){
        if (err){
            res.status(500).send({message: 'Error al procesar la solicitud'});
        }else{
            if (!result){
                res.status(404).send({message: 'Valor incorrecto, no se pudo procesar'});
            }else{
                res.status(200).send({message: result});
            }
        }
    })
}
//Borrar Cliente:
function deleteCliente(req,res){
    var idCliente = req.params.id;
    Cliente.findByIdAndDelete(idCliente, function(err, Cliente){
        if(err){
            return res.json(500,{
                message: 'No se ha encontrado el Cliente a eliminar'
            })
        }
        return res.json(Cliente);
    })
}
//Actualizar Cliente:
function updateCliente(req,res){
    var idCliente = req.params.id;
    Cliente.findOneAndUpdate({_id:idCliente},req.body,{new:true},function(err,Cliente){
        if(err)
            res.send(err)
        res.json(Cliente)
    })
}


module.exports = {
    pruebaCliente,
    saveCliente,
    readCliente,
    listarCliente,
    deleteCliente,
    updateCliente
}