const Producto = require('../models/producto');
function prueba(req, res){
    res.status(200).send({
        message:'Probando la ejecucion del controlador'
    });
}
//Metodos del CRUD

//Funcion crear:
function saveProducto(req, res){
    var myProducto = new Producto(req.body);
    myProducto.save((err, result)=>{
        res.status(200).send({message: result});
    });
}

//Funcion Leer Read:
function readProducto(req,res){
    var idProducto = req.params.id;
    Producto.findById(idProducto).exec((err, result)=>{
        if (err){
            res.status(500).send({message: 'Error al procesar la solicitud'});
        }else{
            if (!result){
                res.status(404).send({message: 'Valor incorrecto. No se pudo procesar'});
            } else {
                res.status(200).send({message: result});
            }
        }
    })

}

function listarProducto(req,res){
    var idProducto = req.params.id;
    if (!idProducto){
        var result=Producto.find({}).sort('nombre');
    }else{
        var result = Producto.find({_id:idProdcuto}).sort('nombre');
    }
    result.exec(function(err,result){
        if (err){
            res.status(500).send({message: 'Error al procesar la solicitud'});
        }else{
            if (!result){
                res.status(404).send({message: 'Valor incorrecto. No se pudo procesar'});
            } else {
                res.status(200).send({message: result});
            }
        }
    })
}
function updateProducto(req,res){
    var idProducto = req.params.id;
    Producto.findOneAndUpdate({_id:idProducto},req.body,{new:true},function(err,Producto){
        if(err)
            res.send(err)
        res.json(Producto)
    })
}
function deleteProducto(req,res){
    var idProducto = req.params.id;
    Producto.findByIdAndDelete(idProducto,function(err,Producto){
        if(err){
            return res.json(500,{
                message: 'No se ha encontrado el Producto a eliminar'
            })
        }
        return res.json(Producto);
    })
}
    const deleteProductoinCar = async (req, res) => {
        const { productoId } = req.params;
      
        /* Buscamos el producto en el carrito */
        const productoInCar = await carshop.findById(productoId);
      
        /* Buscamos el producto en nuestra DB por el nombre del que esta en el carrito */
        const { nombre, precio, presentacion,unidades,_id } = await producto.findOne({
          nombre: productoInCar.nombre,
        });
        
        /* Buscamos y eliminamos el producto con la id */
        await carshop.findByIdAndDelete(productoId);
        
        /* Buscamos y editamos la prop inCart: false */
        /* Le pasamos la id del producto en la DB */
        /* La prop a cambiar y las demas */
        /* Y el new para devolver el producto editado */
        await producto.findByIdAndUpdate(
          _id,
          { isCarShop: false, nombre,codProducto ,presentacion,unidades,img, precio },
          { new: true }
        )
          .then((producto) => {
            res.json({
              mensaje: `El producto ${producto.nombre} fue eliminado del carrito`,
            });
          })
          .catch((error) => res.json({ mensaje: "Hubo un error"+error }));
      };
      


module.exports = {
    prueba, 
    saveProducto,
    readProducto,
    listarProducto,
    updateProducto,
    deleteProducto,
    deleteProductoinCar
}
