const Pedido = require('../models/pedido');

//Metodos del CRUD

//Funcion crear:
function savePedido(req, res){
     var pedidofecha = req.body;
     pedidofecha.fecha = new Date();

    var myPedido = new Pedido(req.body);
    myPedido.save((err, result)=>{
        res.status(200).send({message: result});
    });
}

//Funcion Leer Read:
function readPedido(req,res){
    var idPedido = req.params.id;
    Pedido.findById(idPedido).exec((err, result)=>{
        if (err){
            res.status(500).send({message: 'Error al procesar la solicitud'});
        }else{
            if (!result){
                res.status(404).send({message: 'Valor incorrecto. No se pudo procesar'});
            } else {
                res.status(200).send({message: result});
            }
        }
    })

}

function listarPedido(req,res){
    var idPedido = req.params.id;
    if (!idPedido){
        var result=Pedido.find({}).sort('codPedido');
    }else{
        var result = Pedido.find({_id:idPedido}).sort('codPedido');
    }
    result.exec(function(err,result){
        if (err){
            res.status(500).send({message: 'Error al procesar la solicitud'});
        }else{
            if (!result){
                res.status(404).send({message: 'Valor incorrecto. No se pudo procesar'});
            } else {
                res.status(200).send({message: result});
            }
        }
    })
}
function updatePedido(req,res){
    var idPedido = req.params.id;
    Pedido.findOneAndUpdate({_id:idPedido},req.body,{new:true},function(err,Pedido){
        if(err)
            res.send(err)
        res.json(Pedido)
    })
}
function deletePedido(req,res){
    var idPedido = req.params.id;
    Pedido.findByIdAndDelete(idPedido,function(err,Pedido){
        if(err){
            return res.json(500,{
                message: 'No se ha encontrado el Producto a eliminar'
            })
        }
        return res.json(Pedido);
    })
}

module.exports = {
    savePedido,
    readPedido,
    listarPedido,
    updatePedido,
    deletePedido
}

