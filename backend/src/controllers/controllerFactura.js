const Factura = require('../models/factura');

function fprueba(req, res){
    res.status(200).send({
        message: 'Probando el controlador Factura'
    });
}

// Metodos del CRUD:

//Funcion crear:
function saveFactura(req, res){
    let factura = req.body;
    factura.fechaEmision= new Date();

    //Aumento de dias para la fecha de pago
    var fechaBaseAumentar = new Date();
    var fechaModificadaPago = fechaBaseAumentar.setDate(fechaBaseAumentar.getDate()+10)

    factura.fechaPago=(fechaModificadaPago);
  
   
  
//    fechap =(req.body)
//  let fechap =new Date();
//  fechap.fechaPago.setDate(fechap.getDate() + 5);
    

    var myFactura = new Factura(req.body);
    myFactura.save((err, result)=>{
        res.status(200).send({message: result});
    });
}

//Funcion Leer Read:
function readFactura(req,res){
    var idFactura = req.params.id;
    Factura.findById(idFactura).exec((err, result)=>{
        if (err){
            res.status(500).send({message: 'Error al procesar la solicitud'});
        }else{
            if (!result){
                res.status(404).send({message: 'Valor incorrecto. No se pudo procesar'});
            } else {
                res.status(200).send({message: result});
            }
        }
    })

}

function listarFactura(req,res){
    var idFactura = req.params.id;
    if (!idFactura){
        var result = Factura.find({}).sort('fecha');
    }else{
        var result = Factura.find({_id:idFactura}).sort('fecha');
    }
    result.exec(function(err,result){
        if (err){
            res.status(500).send({message: 'Error al procesar la solicitud'});
        }else{
            if (!result){
                res.status(404).send({message: 'Valor incorrecto. No se pudo procesar'});
            } else {
                res.status(200).send({message: result});
            }
        }
    })
}
function updateFactura(req,res){
    var idFactura = req.params.id;
    Factura.findOneAndUpdate({_id:idFactura},req.body,{new:true},function(err,Factura){
        if(err)
            res.send(err)
        res.json(Factura)
    })
}
function deleteFactura(req,res){
    var idFactura = req.params.id;
    Factura.findByIdAndDelete(idFactura,function(err,Factura){
        if(err){
            return res.json(500,{
                message: 'No se ha encontrado la Factura a eliminar'
            })
        }
        return res.json(Factura);
    })
}

module.exports ={
    fprueba,
    saveFactura,
    readFactura,
    listarFactura,
    updateFactura,
    deleteFactura
}