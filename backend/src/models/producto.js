var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var productoSchema = Schema({
    nombre: String,
    codProducto: Number,
    presentacion: String,
    precio: Number,
    isCarShop:{type:Boolean,default:false},
    unidades:Number
});

const producto = mongoose.model('tbc_producto', productoSchema);
module.exports = producto;