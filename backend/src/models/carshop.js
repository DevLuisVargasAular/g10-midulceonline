var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var carshopSchema = Schema({
    nombre: { type: String, required: true, unique: true },
  //pedido
  //usuario
  //producto
  cantidad: { type: Number, required: true },
  precio: { type: Number, required: true },
});


const carshop = mongoose.model('tbc_carshop',carshopSchema);
module.exports = carshop;
