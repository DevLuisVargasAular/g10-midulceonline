var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var pedidoSchema = Schema({
    codPedido: Number,
    fecha: Date,
    codCliente: Number,
    detallePedido: {

      codProducto: Number,
      precio: Number,
      cantidad: Number,
      subTotal: Number
    },
    precio: Number
})
const pedido = mongoose.model('tbc_pedido',pedidoSchema);
module.exports = pedido;