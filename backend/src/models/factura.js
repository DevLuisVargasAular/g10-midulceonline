var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var facturaSchema = Schema({

    codPedido:Number ,
    fechaEmision: Date,
    fechaPago: Date,
    codFactura: Number,
    estado: String       
})

const factura = mongoose.model('tbc_factura',facturaSchema);
module.exports = factura;
