var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var clienteSchema = Schema({
    nombre: String,
    identificacion: Number,
    codCliente: Number,
    tipoIdentificacion: String,
    direccion: String,
    ciudad: String,
    departamento: String,
    pais: String,
    codPostal: String,
    telefono: String,
    email: String
});

const cliente = mongoose.model('tbc_cliente',clienteSchema);
module.exports = cliente;
