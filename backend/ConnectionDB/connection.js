//declaracion de constante y configuracion de dependencia
const mongoose = require('mongoose');

//conexion
mongoose.connect("mongodb://localhost:27017/MiDulceOnline", {
    useNewUrlParser:true,
    //useCreateindex: true,
    useUnifiedTopology:true,
   // useFindAndModify:false
}, (err,res)=>{
    if(err){
        throw err
    }else{
        console.log('La conexion a la BD se realizo de forma correcta');
    }
}
);
module.exports = mongoose; //Exportacion del archivo para su uso desde otras partes del proyecto