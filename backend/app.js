//carga de las dependencias
var express = require("express");
var app = express();

//carga y uso de los cors se deben instalar antes
const cors = require("cors");
app.use(cors());

var bodyParser = require("body-parser");
//var methodOverride = require("method-override");

var mongoose = require("mongoose");

//uso de las dependencias
app.use(express.json());
app.use(express.urlencoded({
    extended:true
}));
//configuracion de la cabecera y cors
//ejecucion de tipo promesa(()=>{})

app.use((req, res, next)=>{
    res.header('Access-Control-Allow-origin', '*')
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, XRequested-With, Cotent-Type, Accept, Acces-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Request-Method', 'GET, POST, PUT, DELETE, OPTIONS');
    res.header('Allow', 'GET, POST, PUT, DELETE, OPTIONS');
    next();
});

//configuracion del archivo routers.js
app.use(require('./routers/routers'));

//exportacion del archivo app.js
module.exports = app;